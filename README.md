# libflatpak-rs

The Rust bindings of [libflatpak](https://docs.flatpak.org/en/latest/libflatpak-api-reference.html)

Website: <https://world.pages.gitlab.gnome.org/Rust/libflatpak-rs>

## Documentation

- libflatpak: <https://world.pages.gitlab.gnome.org/Rust/libflatpak-rs/stable/latest/docs/libflatpak>
- libflatpak-sys: <https://world.pages.gitlab.gnome.org/Rust/libflatpak-rs/stable/latest/docs/libflatpak_sys>
