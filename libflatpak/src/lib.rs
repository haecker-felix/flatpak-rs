#![cfg_attr(docsrs, feature(doc_cfg))]
#![allow(clippy::too_many_arguments)]
#![allow(clippy::type_complexity)]

// Re-export the -sys bindings
pub use ffi;
// Re-export bindings
pub use gio;
pub use glib;

macro_rules! assert_initialized_main_thread {
    () => {};
}

macro_rules! skip_assert_initialized {
    () => {};
}

mod auto;
pub use auto::*;

pub mod prelude;

pub mod functions {
    pub use super::auto::functions::*;
}
pub use functions::*;

pub mod builders {
    pub use super::auto::builders::*;
}

mod installation;
mod transaction;
