// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::{ffi, TransactionOperationType};
use glib::translate::*;

glib::wrapper! {
    #[doc(alias = "FlatpakTransactionOperation")]
    pub struct TransactionOperation(Object<ffi::FlatpakTransactionOperation, ffi::FlatpakTransactionOperationClass>);

    match fn {
        type_ => || ffi::flatpak_transaction_operation_get_type(),
    }
}

impl TransactionOperation {
    #[doc(alias = "flatpak_transaction_operation_get_bundle_path")]
    #[doc(alias = "get_bundle_path")]
    pub fn bundle_path(&self) -> Option<gio::File> {
        unsafe {
            from_glib_none(ffi::flatpak_transaction_operation_get_bundle_path(
                self.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "flatpak_transaction_operation_get_commit")]
    #[doc(alias = "get_commit")]
    pub fn commit(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::flatpak_transaction_operation_get_commit(
                self.to_glib_none().0,
            ))
        }
    }

    #[cfg(feature = "v1_1_2")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_1_2")))]
    #[doc(alias = "flatpak_transaction_operation_get_download_size")]
    #[doc(alias = "get_download_size")]
    pub fn download_size(&self) -> u64 {
        unsafe { ffi::flatpak_transaction_operation_get_download_size(self.to_glib_none().0) }
    }

    #[cfg(feature = "v1_1_2")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_1_2")))]
    #[doc(alias = "flatpak_transaction_operation_get_installed_size")]
    #[doc(alias = "get_installed_size")]
    pub fn installed_size(&self) -> u64 {
        unsafe { ffi::flatpak_transaction_operation_get_installed_size(self.to_glib_none().0) }
    }

    #[cfg(feature = "v1_7_3")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_7_3")))]
    #[doc(alias = "flatpak_transaction_operation_get_is_skipped")]
    #[doc(alias = "get_is_skipped")]
    pub fn is_skipped(&self) -> bool {
        unsafe {
            from_glib(ffi::flatpak_transaction_operation_get_is_skipped(
                self.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "flatpak_transaction_operation_get_metadata")]
    #[doc(alias = "get_metadata")]
    pub fn metadata(&self) -> Option<glib::KeyFile> {
        unsafe {
            from_glib_none(ffi::flatpak_transaction_operation_get_metadata(
                self.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "flatpak_transaction_operation_get_old_metadata")]
    #[doc(alias = "get_old_metadata")]
    pub fn old_metadata(&self) -> Option<glib::KeyFile> {
        unsafe {
            from_glib_none(ffi::flatpak_transaction_operation_get_old_metadata(
                self.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "flatpak_transaction_operation_get_operation_type")]
    #[doc(alias = "get_operation_type")]
    pub fn operation_type(&self) -> TransactionOperationType {
        unsafe {
            from_glib(ffi::flatpak_transaction_operation_get_operation_type(
                self.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "flatpak_transaction_operation_get_ref")]
    pub fn get_ref(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::flatpak_transaction_operation_get_ref(
                self.to_glib_none().0,
            ))
        }
    }

    #[cfg(feature = "v1_7_3")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_7_3")))]
    #[doc(alias = "flatpak_transaction_operation_get_related_to_ops")]
    #[doc(alias = "get_related_to_ops")]
    pub fn related_to_ops(&self) -> Vec<TransactionOperation> {
        unsafe {
            FromGlibPtrContainer::from_glib_none(
                ffi::flatpak_transaction_operation_get_related_to_ops(self.to_glib_none().0),
            )
        }
    }

    #[doc(alias = "flatpak_transaction_operation_get_remote")]
    #[doc(alias = "get_remote")]
    pub fn remote(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::flatpak_transaction_operation_get_remote(
                self.to_glib_none().0,
            ))
        }
    }

    #[cfg(feature = "v1_9_1")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_9_1")))]
    #[doc(alias = "flatpak_transaction_operation_get_requires_authentication")]
    #[doc(alias = "get_requires_authentication")]
    pub fn requires_authentication(&self) -> bool {
        unsafe {
            from_glib(
                ffi::flatpak_transaction_operation_get_requires_authentication(
                    self.to_glib_none().0,
                ),
            )
        }
    }

    #[cfg(feature = "v1_9_1")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_9_1")))]
    #[doc(alias = "flatpak_transaction_operation_get_subpaths")]
    #[doc(alias = "get_subpaths")]
    pub fn subpaths(&self) -> Vec<glib::GString> {
        unsafe {
            FromGlibPtrContainer::from_glib_none(ffi::flatpak_transaction_operation_get_subpaths(
                self.to_glib_none().0,
            ))
        }
    }
}
