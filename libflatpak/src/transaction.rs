use crate::{
    prelude::*, Transaction, TransactionErrorDetails, TransactionOperation, TransactionResult,
};
use glib::{
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, mem::transmute};

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::Transaction>> Sealed for T {}
}

pub trait TransactionExtManual: sealed::Sealed + IsA<Transaction> + 'static {
    #[doc(alias = "operation-done")]
    fn connect_operation_done<
        F: Fn(&Self, &TransactionOperation, &str, TransactionResult) + 'static,
    >(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn operation_done_trampoline<
            P: IsA<Transaction>,
            F: Fn(&P, &TransactionOperation, &str, TransactionResult) + 'static,
        >(
            this: *mut ffi::FlatpakTransaction,
            operation: *mut ffi::FlatpakTransactionOperation,
            commit: *mut libc::c_char,
            result: libc::c_int,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(
                Transaction::from_glib_borrow(this).unsafe_cast_ref(),
                &from_glib_borrow(operation),
                &glib::GString::from_glib_borrow(commit),
                from_glib(result as u32),
            )
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"operation-done\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    operation_done_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "operation-error")]
    fn connect_operation_error<
        F: Fn(&Self, &TransactionOperation, &glib::Error, TransactionErrorDetails) -> bool + 'static,
    >(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn operation_error_trampoline<
            P: IsA<Transaction>,
            F: Fn(&P, &TransactionOperation, &glib::Error, TransactionErrorDetails) -> bool + 'static,
        >(
            this: *mut ffi::FlatpakTransaction,
            operation: *mut ffi::FlatpakTransactionOperation,
            error: *mut glib::ffi::GError,
            details: libc::c_int,
            f: glib::ffi::gpointer,
        ) -> glib::ffi::gboolean {
            let f: &F = &*(f as *const F);
            f(
                Transaction::from_glib_borrow(this).unsafe_cast_ref(),
                &from_glib_borrow(operation),
                &from_glib_borrow(error),
                from_glib(details as u32),
            )
            .into_glib()
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"operation-error\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    operation_error_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl<O: IsA<Transaction>> TransactionExtManual for O {}
